package com.example.ycaro.appycaro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CadastroActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button buttonCad = (Button) findViewById(R.id.botaoCad);

        buttonCad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editTextProduto = (EditText) findViewById(R.id.nome_produto);
                String nomeProduto = editTextProduto.getText().toString();

                EditText editTextQtde = (EditText) findViewById(R.id.qtde);
                String qtde = editTextQtde.getText().toString();

                Produto produto = new Produto();
                produto.setNome(nomeProduto);
                produto.setQtde(qtde);

                ProdutosDB pDB = new ProdutosDB(getContext());
                pDB.save(produto);

                alert("Produto Cadastrado!");
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void alert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
    public Context getContext() {
        return this;
    }
}
