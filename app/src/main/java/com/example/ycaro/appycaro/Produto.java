package com.example.ycaro.appycaro;

public class Produto {

    int id;
    String nome,qtde;

    public Produto(){}

    public Produto(String nome){
        this.nome = nome;
    }

    public Produto(int id, String nome, String qtde){
        this.id = id;
        this.nome = nome;
        this.qtde = qtde;
    }

    public Produto( String nome, String qtde){
        this.nome = nome;
        this.qtde = qtde;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQtde() {
        return qtde;
    }

    public void setQtde(String qtde) {
        this.qtde = qtde;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String toString(){
        return "Produto: "+this.nome+" "+this.qtde;
    }
}
